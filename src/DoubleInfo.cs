﻿namespace FuzzyLogic
{
	/// <summary>	Detailed information about a <see cref="double"/>. </summary> 
	public struct DoubleInfo
	{
		private readonly double _value;
		private readonly long _bits;

		/// <summary>	Constructs a new <see cref="DoubleInfo"/> for <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value to obtain info about. </param>
		public DoubleInfo(double value)
		{
			_value = value;
			_bits = DoubleToInt64Bits(value);
		}

		/// <summary>	The stored value. </summary>
		///
		/// <value>	The stored value. </value>
		public double Value
		{
			get { return _value; }
		}

		/// <summary>	The underlying bit representation of the value, returned as a <see cref="long"/>. </summary>
		///
		/// <value>	The underlying bit representation of the value, returned as a <see cref="long"/>. </value>
		public long Bits
		{
			get { return _bits; }
		}

		/// <summary>	Gets a flag indicating whether the value is negative. </summary>
		///
		/// <value>	true if the value is negative, false if not. </value>
		public bool IsNegative
		{
			get { return (_bits < 0); }
		}

		/// <summary>	The exponent component of the value. </summary>
		///
		/// <value>	The exponent. </value>
		public int Exponent
		{
			get { return (int)((_bits >> 52) & 0x7ffL); }
		}

		/// <summary>	The mantissa component of the value. </summary>
		///
		/// <value>	The mantissa. </value>
		public long Mantissa
		{
			get { return _bits & 0xfffffffffffffL; }
		}

		/// <summary>	Converts a <see cref="double"/> into a <see cref="long"/>, preserving the exact binary representation
		/// 			of the <see cref="double"/>. </summary>
		///
		/// <param name="value">	The value to convert. </param>
		///
		/// <returns>	A <see cref="long"/> that stores the exact binary representation of the given value. </returns>
		public static long DoubleToInt64Bits(double value)
		{
			// This allows us to perform the same operations as BitConverter, but without allocating a new array of bytes. 
			return new DoubleUnion(value).LongValue;
		}
	}
}
