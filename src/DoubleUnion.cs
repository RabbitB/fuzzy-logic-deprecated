﻿using System;
using System.Runtime.InteropServices;
using FuzzyLogic.Properties;

namespace FuzzyLogic
{
	/// <summary>	Acts as a C union between a <see cref="double"/> and other 64-bit values. </summary>
	///
	/// <remarks>	Set one value, and you can retrieve the others as the raw binary representation of the set value. </remarks>
	[StructLayout(LayoutKind.Explicit)]
	public struct DoubleUnion
	{
		/// <summary>	The stored value as a <see cref="double"/>. </summary>
		[FieldOffset(0)] public readonly double DoubleValue;

		/// <summary>	The stored value as a <see cref="long"/>. </summary>
		[FieldOffset(0)] public readonly long LongValue;

		/// <summary>	The stored value as a <see cref="ulong"/>. </summary> 
		[FieldOffset(0)] public readonly ulong ULongValue;

		/// <summary>	Constructs a <see cref="DoubleUnion"/> with a <see cref="double"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as a <see cref="double"/>. </param>
		public DoubleUnion(double value) : this()
		{
			DoubleValue = value;
		}

		/// <summary>	Constructs a <see cref="DoubleUnion"/> with a <see cref="long"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as a <see cref="long"/>. </param>
		public DoubleUnion(long value) : this()
		{
			LongValue = value;
		}

		/// <summary>	Constructs a <see cref="DoubleUnion"/> with a <see cref="ulong"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as a <see cref="ulong"/>. </param>
		public DoubleUnion(ulong value) : this()
		{
			ULongValue = value;
		}

		/// <summary>	Breaks the stored value down into its representative <see cref="byte"/> array. </summary>
		///
		/// <remarks>	The use of a pre-allocated buffer allows this method to function without allocating any data.
		/// 			<see cref="BitConverter.GetBytes(Double)"/> will always allocate a new <see cref="byte"/> array, which
		/// 			is why this method was written. </remarks>
		///
		/// <exception cref="ArgumentException">	Thrown when the size of the supplied <see cref="byte"/> buffer is smaller
		/// 										than the required size. </exception>
		///
		/// <param name="buffer">	The <see cref="byte"/> buffer to fill. </param>
		///
		/// <returns>	The <see cref="byte"/> buffer filled with the value broken into bytes. </returns>
		public byte[] GetBytes(byte[] buffer)
		{
			if (buffer.Length < sizeof(double))
			{
				throw new ArgumentException(
					String.Format(ExceptionStrings.DoubleUnion_BufferNotLargeEnough, buffer.Length, sizeof(double)),
					"buffer");
			}

			buffer[0] = (byte)(LongValue & 0xFF);
			buffer[1] = (byte)((LongValue >> 8) & 0xFF);
			buffer[2] = (byte)((LongValue >> 16) & 0xFF);
			buffer[3] = (byte)((LongValue >> 24) & 0xFF);
			buffer[4] = (byte)((LongValue >> 32) & 0xFF);
			buffer[5] = (byte)((LongValue >> 40) & 0xFF);
			buffer[6] = (byte)((LongValue >> 48) & 0xFF);
			buffer[7] = (byte)((LongValue >> 56) & 0xFF);

			return buffer;
		}
	}
}
