﻿namespace FuzzyLogic.Extensions
{
	/// <summary>	Collection of extension methods that allow any <see cref="float"/> or <see cref="double"/> to be converted into a
	/// <see cref="FuzzySingle"/> or <see cref="FuzzyDouble"/>, respectively. </summary> 
	public static class FuzzyLogicExtensions
	{
		/// <summary>	Creates a <see cref="FuzzySingle"/> from a <see cref="float"/>. </summary>
		///
		/// <param name="value">	   	The value to create a <see cref="FuzzySingle"/> from. </param>
		/// <param name="ulpTolerance">	The maximum ULP tolerance. </param>
		///
		/// <returns>	A <see cref="FuzzySingle"/>. </returns>
		public static FuzzySingle Fuzzy(this float value, int ulpTolerance = FuzzySingle.DefaultULPTolerance)
		{
			return new FuzzySingle(value, ulpTolerance);
		}

		/// <summary>	Creates a <see cref="FuzzySingle"/> from a <see cref="float"/>. </summary>
		///
		/// <param name="value">			The value to create a <see cref="FuzzySingle"/> from. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	A <see cref="FuzzySingle"/>. </returns>
		public static FuzzySingle Fuzzy(this float value, float marginOfError, int ulpTolerance)
		{
			return new FuzzySingle(value, marginOfError, ulpTolerance);
		}

		/// <summary>	Creates a <see cref="FuzzyDouble"/> from a <see cref="double"/>. </summary>
		///
		/// <param name="value">	   	The value to create a <see cref="FuzzyDouble"/> from. </param>
		/// <param name="ulpTolerance">	The maximum ULP tolerance. </param>
		///
		/// <returns>	A <see cref="FuzzyDouble"/>. </returns>
		public static FuzzyDouble Fuzzy(this double value, int ulpTolerance = FuzzyDouble.DefaultULPTolerance)
		{
			return new FuzzyDouble(value, ulpTolerance);
		}

		/// <summary>	Creates a <see cref="FuzzyDouble"/> from a <see cref="double"/>. </summary>
		///
		/// <param name="value">			The value to create a <see cref="FuzzyDouble"/> from. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	A <see cref="FuzzyDouble"/>. </returns>
		public static FuzzyDouble Fuzzy(this double value, double marginOfError, int ulpTolerance)
		{
			return new FuzzyDouble(value, marginOfError, ulpTolerance);
		}
	}
}
