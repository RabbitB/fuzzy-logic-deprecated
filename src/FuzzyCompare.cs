﻿using System;
using FuzzyLogic.Properties;

namespace FuzzyLogic
{
	/// <summary>	A static class that allows for fuzzy comparison between floating point numbers, and calculating ULP based
	/// 			information from numbers. </summary>
	public static class FuzzyCompare
	{
		/// <summary>	The difference between <see cref="Single.MaxValue"/> and a float that is one ULP smaller. </summary> 
		public const float MaxFloatULP = 2.0282409603651670423947251286016E+31f;

		/// <summary>	The difference between <see cref="Double.MaxValue"/> and a double that is one ULP smaller. </summary> 
		public const double MaxDoubleULP = 1.9958403095347198116563727130368E+292;

		/// <summary>	Tests if two floating point numbers are equal, using fuzzy logic. </summary>
		///
		/// <remarks>	This method makes use of both an absolute margin of error and a ULP tolerance. If the difference
		/// 			between two values is equal to or smaller than the margin of error, the two values are equal. If
		/// 			they're outside the margin of error, then the difference in ULP between the numbers is calculated. One
		/// 			ULP is the smallest representable change between two floating point numbers, and the actual size of the
		/// 			ULP changes with the scale of the values. This means that ULP can more accurately compare two values
		/// 			for equality than a straight epsilon, or difference comparison. Unfortunately it breaks down near 0 and
		/// 			in several other edge cases, which is why the margin of error check is performed first. For more
		/// 			information on how this method is implemented, see
		/// 			http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/. </remarks>
		///
		/// <param name="first">			The first value to be compared. </param>
		/// <param name="second">			The second value to be compared. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	true if the values are considered equal; otherwise, false. </returns>
		public static bool AreEqual(float first, float second, float marginOfError, int ulpTolerance)
		{
			// When numbers are very close to zero, this initial check of absolute values is needed. Otherwise 
			// we can safely use the ULP difference.
			float absoluteDiff = Math.Abs(first - second);
			if (absoluteDiff <= marginOfError) { return true; }

			SingleInfo firstInfo = new SingleInfo(first);
			SingleInfo secondInfo = new SingleInfo(second);

			// Different signs mean the numbers don't match, period.
			if (firstInfo.IsNegative != secondInfo.IsNegative) { return false; }

			// Find the difference in ULPs (unit of least precision).
			int ulpDiff = Math.Abs(firstInfo.Bits - secondInfo.Bits);
			return ulpDiff <= ulpTolerance;
		}

		/// <summary>	Tests if two floating point numbers are equal, using fuzzy logic. </summary>
		///
		/// <remarks>	This method makes use of both an absolute margin of error and a ULP tolerance. If the difference
		/// 			between two values is equal to or smaller than the margin of error, the two values are equal. If
		/// 			they're outside the margin of error, then the difference in ULP between the numbers is calculated. One
		/// 			ULP is the smallest representable change between two floating point numbers, and the actual size of the
		/// 			ULP changes with the scale of the values. This means that ULP can more accurately compare two values
		/// 			for equality than a straight epsilon, or difference comparison. Unfortunately it breaks down near 0 and
		/// 			in several other edge cases, which is why the margin of error check is performed first. For more
		/// 			information on how this method is implemented, see
		/// 			http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/. </remarks>
		///
		/// <param name="first">			The first value to be compared. </param>
		/// <param name="second">			The second value to be compared. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	true if the values are considered equal; otherwise, false. </returns>
		public static bool AreEqual(double first, double second, double marginOfError, int ulpTolerance)
		{
			// When numbers are very close to zero, this initial check of absolute values is needed. Otherwise 
			// we can safely use the ULP difference.
			double absoluteDiff = Math.Abs(first - second);
			if (absoluteDiff <= marginOfError) { return true; }

			DoubleInfo firstInfo = new DoubleInfo(first);
			DoubleInfo secondInfo = new DoubleInfo(second);

			// Different signs mean the numbers don't match, period.
			if (firstInfo.IsNegative != secondInfo.IsNegative) { return false; }

			// Find the difference in ULPs (unit of least precision).
			long ulpDiff = Math.Abs(firstInfo.Bits - secondInfo.Bits);
			return ulpDiff <= ulpTolerance;
		}

		/// <summary>	Compares two floating point numbers to each other, to determine their relative ordering. </summary>
		///
		/// <param name="first">			The value to compare. </param>
		/// <param name="second">			The value being compared against. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	Negative if 'first' is less than 'second', 0 if they are equal, or positive if it is greater. </returns>
		public static int Compare(float first, float second, float marginOfError, int ulpTolerance)
		{
			if (AreEqual(first, second, marginOfError, ulpTolerance)) { return 0; }
			return first < second ? -1 : 1;
		}

		/// <summary>	Compares two floating point numbers to each other, to determine their relative ordering. </summary>
		///
		/// <param name="first">			The value to compare. </param>
		/// <param name="second">			The value being compared against. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		///
		/// <returns>	Negative if 'first' is less than 'second', 0 if they are equal, or positive if it is greater. </returns>
		public static int Compare(double first, double second, double marginOfError, int ulpTolerance)
		{
			if (AreEqual(first, second, marginOfError, ulpTolerance)) { return 0; }
			return first < second ? -1 : 1;
		}

		/// <summary>	Determines the difference between <paramref name="value"/> and the value of the next larger
		/// 			representable <see cref="float"/> (<paramref name="value"/> + 1 ULP). </summary>
		///
		/// <remarks>	<para>Behaves similar to Java's Math.ulp(float value).</para>
		/// 			
		/// 			<list type="bullet">
		/// 			<listheader>
		/// 				<description>Special Cases:</description>
		/// 			</listheader>
		/// 			<item>
		/// 				<description>If the argument is <see cref="float.NaN"/>, then the result is
		/// 				<see cref="float.NaN"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is <see cref="float.PositiveInfinity"/> or
		/// 				<see cref="float.NegativeInfinity"/>, then the result is
		/// 				<see cref="float.PositiveInfinity"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is positive or negative zero, then the result is
		/// 				<see cref="float.Epsilon"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is <see cref="float.MaxValue"/> or <see cref="float.MinValue"/>, then the
		/// 				result is equal to <see cref="MaxFloatULP"/></description>
		/// 			</item>
		/// 			</list> </remarks>
		///
		/// <param name="value">	The value to determine the ULP difference of. </param>
		///
		/// <returns>	The ULP size for the given value. </returns>
		public static float ULP(float value)
		{
			if (Single.IsNaN(value)) { return Single.NaN; }
			if (Single.IsPositiveInfinity(value) || Single.IsNegativeInfinity(value)) { return Single.PositiveInfinity; }
			if (value.Equals(0.0f)) { return Single.Epsilon; }
			if (Math.Abs(value).Equals(Single.MaxValue)) { return MaxFloatULP; }

			SingleInfo info = new SingleInfo(value);
			SingleUnion union = new SingleUnion(info.Bits + 1);

			return Math.Abs(union.FloatValue - value);
		}

		/// <summary>	Determines the difference between <paramref name="value"/> and the value of the next larger
		/// 			representable <see cref="double"/> (<paramref name="value"/> + 1 ULP). </summary>
		///
		/// <remarks>	<para>Behaves similar to Java's Math.ulp(float value).</para>
		/// 			
		/// 			<list type="bullet">
		/// 			<listheader>
		/// 				<description>Special Cases:</description>
		/// 			</listheader>
		/// 			<item>
		/// 				<description>If the argument is <see cref="double.NaN"/>, then the result is
		/// 				<see cref="double.NaN"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is <see cref="double.PositiveInfinity"/> or
		/// 				<see cref="double.NegativeInfinity"/>, then the result is
		/// 				<see cref="double.PositiveInfinity"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is positive or negative zero, then the result is
		/// 				<see cref="double.Epsilon"/>.</description>
		/// 			</item>
		/// 			<item>
		/// 				<description>If the argument is <see cref="double.MaxValue"/> or <see cref="double.MinValue"/>, then
		/// 				the result is equal to <see cref="MaxDoubleULP"/></description>
		/// 			</item>
		/// 			</list> </remarks>
		///
		/// <param name="value">	The value to determine the ULP difference of. </param>
		///
		/// <returns>	The ULP size for the given value. </returns>
		public static double ULP(double value)
		{
			if (Double.IsNaN(value)) { return Double.NaN; }
			if (Double.IsPositiveInfinity(value) || Double.IsNegativeInfinity(value)) { return Double.PositiveInfinity; }
			if (value.Equals(0.0)) { return Double.Epsilon; }
			if (Math.Abs(value).Equals(Double.MaxValue)) { return MaxDoubleULP; }

			DoubleInfo info = new DoubleInfo(value);
			DoubleUnion union = new DoubleUnion(info.Bits + 1);

			return Math.Abs(union.DoubleValue - value);
		}

		/// <summary>	Calculates the number of ULP between two numbers. </summary>
		///
		/// <exception cref="ArgumentException">	Thrown when <paramref name="first"/> and <paramref name="second"/> have
		/// 										mixed signs (both must be positive or negative). </exception>
		///
		/// <param name="first"> 	The first number. </param>
		/// <param name="second">	The second number. </param>
		///
		/// <returns>	The number of ULP between the two numbers. </returns>
		public static int ULPDistance(float first, float second)
		{
			SingleInfo firstInfo = new SingleInfo(first);
			SingleInfo secondInfo = new SingleInfo(second);

			if (firstInfo.IsNegative != secondInfo.IsNegative)
			{
				throw new ArgumentException(ExceptionStrings.FuzzyCompare_MixedSignsNotAllowed);
			}

			return Math.Abs(firstInfo.Bits - secondInfo.Bits);
		}

		/// <summary>	Calculates the number of ULP between two numbers. </summary>
		///
		/// <exception cref="ArgumentException">	Thrown when <paramref name="first"/> and <paramref name="second"/> have
		/// 										mixed signs (both must be positive or negative). </exception>
		///
		/// <param name="first"> 	The first number. </param>
		/// <param name="second">	The second number. </param>
		///
		/// <returns>	The number of ULP between the two numbers. </returns>
		public static long ULPDistance(double first, double second)
		{
			DoubleInfo firstInfo = new DoubleInfo(first);
			DoubleInfo secondInfo = new DoubleInfo(second);

			if (firstInfo.IsNegative != secondInfo.IsNegative)
			{
				throw new ArgumentException(ExceptionStrings.FuzzyCompare_MixedSignsNotAllowed);
			}

			return Math.Abs(firstInfo.Bits - secondInfo.Bits);
		}
	}
}
