﻿using System;
using System.Collections.Generic;

namespace FuzzyLogic
{
	/// <summary>	Compares <see cref="double"/> values using fuzzy logic. </summary>
	///
	/// <seealso cref="T:System.Collections.Generic.IEqualityComparer{System.Double}"/>
	/// <seealso cref="T:System.Collections.Generic.IComparer{System.Double}"/>
	public class FuzzyDoubleComparer : IEqualityComparer<double>, IComparer<double>
	{
		private double _marginOfError;
		private int _ulpTolerance;

		/// <summary>	Constructs a comparer that compares <see cref="double"/> values using fuzzy logic. </summary>
		///
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		public FuzzyDoubleComparer(double marginOfError, int ulpTolerance)
		{
			MarginOfError = marginOfError;
			ULPTolerance = ulpTolerance;
		}

		/// <summary>	The margin of error. </summary>
		///
		/// <remarks>	Two values with a difference &lt;= the margin of error, will equate to equal. </remarks>
		///
		/// <value>	The margin of error. </value>
		public double MarginOfError
		{
			get { return _marginOfError; }
			set { _marginOfError = Math.Abs(value); }
		}

		/// <summary>	The ULP tolerance. </summary>
		///
		/// <remarks>	If the difference between two values is larger than <see cref="MarginOfError"/>, they are tested for
		/// 			equality using the number of ULP between them. </remarks>
		///
		/// <value>	The ULP tolerance. </value>
		public int ULPTolerance
		{
			get { return _ulpTolerance; }
			set { _ulpTolerance = Math.Abs(value); }
		}

		/// <summary>	Compares two <see cref="double"/> values against each other to determine their relative ordering. </summary>
		///
		/// <param name="first"> 	The value to compare. </param>
		/// <param name="second">	The value being compared against. </param>
		///
		/// <returns>	Negative if <paramref name="first"/> is less than <paramref name="second"/>, 0 if they are equal, or
		/// 			positive if <paramref name="first"/> is greater. </returns>
		public int Compare(double first, double second)
		{
			if (Equals(first, second)) { return 0; }
			return first < second ? -1 : 1;
		}

		/// <summary>	Determines whether two <see cref="double"/> values are equal to each other, using fuzzy logic. </summary>
		///
		/// <remarks>	The two numbers are considered equal if their difference is &lt;= the margin of error. If the
		/// 			difference is larger than the margin of error, they are considered equal if the ULP difference is &lt;=
		/// 			the allowed ULP tolerance. </remarks>
		///
		/// <param name="first"> 	First value to be compared. </param>
		/// <param name="second">	Second value to be compared. </param>
		///
		/// <returns>	true if the values are considered equal; otherwise, false. </returns>
		public bool Equals(double first, double second)
		{
			return FuzzyCompare.AreEqual(first, second, _marginOfError, _ulpTolerance);
		}

		/// <summary>	Fuzzy floating point values don't support hashing and shouldn't be used in any context that requires
		/// 			it. </summary>
		///
		/// <remarks>	This hash code method only exists to fully implement the <see cref="IEqualityComparer{T}"/> interface,
		/// 			and conforms to the requirement that if A == B, then A.GetHashCode() == B.GetHashCode. It does this by
		/// 			simply returning 0, which technically meets the specification. </remarks>
		///
		/// <param name="value">	The value to hash. </param>
		///
		/// <returns>	An <see cref="int"/> that is the hash code for <paramref name="value"/>. </returns>
		public int GetHashCode(double value)
		{
			return 0;
		}
	}
}
