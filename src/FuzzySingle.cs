﻿using System;

namespace FuzzyLogic
{
	/// <summary>	A <see cref="float"/> value that supports fuzzy logic. </summary>
	///
	/// <remarks>	Does not support hashing. </remarks>
	///
	/// <seealso cref="T:IEquatable{FuzzySingle}"/>
	/// <seealso cref="T:IEquatable{float}"/>
	/// <seealso cref="T:IComparable{FuzzySingle}"/>
	/// <seealso cref="T:IComparable{float}"/>
	public struct FuzzySingle : IEquatable<FuzzySingle>, IEquatable<float>, IComparable<FuzzySingle>, IComparable<float>
	{
		/// <summary>	The default ULP tolerance to use for <see cref="FuzzySingle"/>. </summary> 
		public const int DefaultULPTolerance = 4;

		private readonly float _value;
		private readonly float _marginOfError;
		private readonly int _ulpTolerance;

		/// <summary>	Constructs a <see cref="FuzzySingle"/> with an implicit margin of error that matches the
		/// 			<paramref name="ulpTolerance"/>. </summary>
		///
		/// <param name="value">	   	The value to make fuzzy. </param>
		/// <param name="ulpTolerance">	The maximum ULP tolerance. </param>
		public FuzzySingle(float value, int ulpTolerance = 4)
			: this(value, FuzzyCompare.ULP(value) * ulpTolerance, ulpTolerance) {}

		/// <summary>	Constructs a <see cref="FuzzySingle"/> with an explicit <paramref name="marginOfError"/> and
		/// 			<paramref name="ulpTolerance"/>. </summary>
		///
		/// <param name="value">			The value to make fuzzy. </param>
		/// <param name="marginOfError">	The allowable margin of error. </param>
		/// <param name="ulpTolerance"> 	The maximum ULP tolerance. </param>
		public FuzzySingle(float value, float marginOfError, int ulpTolerance)
		{
			_value = value;

			_marginOfError = Math.Abs(marginOfError);
			_ulpTolerance = Math.Abs(ulpTolerance);
		}

		/// <summary>	The stored value. </summary>
		///
		/// <value>	The stored value. </value>
		public float Value
		{
			get { return _value; }
		}

		/// <summary>	The margin of error. </summary>
		///
		/// <remarks>	Two values with a difference &lt;= the margin of error, will equate to equal. </remarks>
		///
		/// <value>	The margin of error. </value>
		public float MarginOfError
		{
			get { return _marginOfError; }
		}

		/// <summary>	The ULP tolerance. </summary>
		///
		/// <remarks>	If the difference between two values is larger than <see cref="MarginOfError"/>, they are tested for
		/// 			equality using the number of ULP between them. </remarks>
		///
		/// <value>	The ULP tolerance. </value>
		public int ULPTolerance
		{
			get { return _ulpTolerance; }
		}

		#region Equality

		/// <summary>	Tests if this instance is considered equal to another <see cref="FuzzySingle"/>. </summary>
		///
		/// <remarks>	When two <see cref="FuzzySingle"/> are compared to each other, the parameters (margin of error and ULP
		/// 			tolerance) of the first operand are used for the comparison, and the parameters of the second operand
		/// 			are ignored. The two numbers are considered equal if their difference is &lt;= the margin of error. If
		/// 			the difference is larger than the margin of error, they are considered equal if the ULP difference is
		/// 			&lt;= the allowed ULP tolerance. </remarks>
		///
		/// <param name="other">	The <see cref="FuzzySingle"/> to compare to this instance. </param>
		///
		/// <returns>	true if this instance and <paramref name="other"/> are considered equal; otherwise, false. </returns>
		public bool Equals(FuzzySingle other)
		{
			return FuzzyCompare.AreEqual(_value, other._value, _marginOfError, _ulpTolerance);
		}

		/// <summary>	Tests if this instance is considered equal to a <see cref="float"/>. </summary>
		///
		/// <remarks>	When a <see cref="FuzzySingle"/> is compared to a <see cref="float"/>, the parameters (margin of error
		/// 			and ULP tolerance) of the <see cref="FuzzySingle"/> are used for the comparison. The two numbers are
		/// 			considered equal if their difference is &lt;= the margin of error. If the difference is larger than the
		/// 			margin of error, they are considered equal if the ULP difference is &lt;= the allowed ULP tolerance. </remarks>
		///
		/// <param name="other">	The <see cref="float"/> to compare to this instance. </param>
		///
		/// <returns>	true if the value of this instance and <paramref name="other"/> are considered equal; otherwise, false. </returns>
		public bool Equals(float other)
		{
			return FuzzyCompare.AreEqual(_value, other, _marginOfError, _ulpTolerance);
		}

		/// <summary>	Tests if this instance is considered equal to an <see cref="object"/>. </summary>
		///
		/// <param name="obj">	The object to compare to this instance. </param>
		///
		/// <returns>	true if <paramref name="obj"/> is either a <see cref="FuzzySingle"/> or <see cref="float"/>, and
		/// 			represents a value that is considered equal to the value of this instance; otherwise, false. </returns>
		///
		/// <seealso cref="M:System.ValueType.Equals(object)"/>
		public override bool Equals(object obj)
		{
			if (obj is FuzzySingle) { return Equals((FuzzySingle)obj); }
			if (obj is float) { return Equals((float)obj); }

			return false;
		}

		/// <summary>	Equality operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is considered equal to <paramref name="second"/>; otherwise, false. </returns>
		public static bool operator ==(FuzzySingle first, FuzzySingle second)
		{
			return first.Equals(second);
		}

		/// <summary>	Equality operator for comparing a <see cref="FuzzySingle"/> to a <see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is considered equal to <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator ==(FuzzySingle first, float second)
		{
			return first.Equals(second);
		}

		/// <summary>	Equality operator for comparing a <see cref="float"/> to a <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is considered equal to the value of <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator ==(float first, FuzzySingle second)
		{
			return second.Equals(first);
		}

		/// <summary>	Inequality operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is not considered equal to <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator !=(FuzzySingle first, FuzzySingle second)
		{
			return !(first.Equals(second));
		}

		/// <summary>	Inequality operator for comparing a <see cref="FuzzySingle"/> to a <see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is not considered equal to <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator !=(FuzzySingle first, float second)
		{
			return !(first.Equals(second));
		}

		/// <summary>	Inequality operator for comparing a <see cref="float"/> to a <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is not considered equal to the value of <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator !=(float first, FuzzySingle second)
		{
			return !(second.Equals(first));
		}

		/// <summary>	This struct doesn't support hashing and shouldn't be used in any context that requires it. </summary>
		///
		/// <remarks>	This hash code method only exists to fully implement the <see cref="IEquatable{T}"/> interface, and
		/// 			conforms to the requirement that if A == B, then A.GetHashCode() == B.GetHashCode. It does this by
		/// 			simply returning 0, which technically meets the specification. </remarks>
		///
		/// <returns>	A 32-bit signed integer that is the hash code for this instance. </returns>
		///
		/// <seealso cref="M:System.ValueType.GetHashCode()"/>
		public override int GetHashCode()
		{
			return 0;
		}

		#endregion

		#region Comparison

		/// <summary>	Compares this instance to another <see cref="FuzzySingle"/> to determine their relative ordering. </summary>
		///
		/// <remarks>	When two <see cref="FuzzySingle"/> are compared to each other, the parameters (margin of error and ULP
		/// 			tolerance) of the first operand are used for the comparison, and the parameters of the second operand
		/// 			are ignored. </remarks>
		///
		/// <param name="other">	The <see cref="FuzzySingle"/> to compare against. </param>
		///
		/// <returns>	Negative if this instance is less than <paramref name="other"/>, 0 if they are equal, or positive if
		/// 			this is greater. </returns>
		public int CompareTo(FuzzySingle other)
		{
			if (Equals(other)) { return 0; }
			return _value < other._value ? -1 : 1;
		}

		/// <summary>	Compares the value of this instance to a <see cref="float"/> to determine their relative ordering. </summary>
		///
		/// <remarks>	When a <see cref="FuzzySingle"/> is compared to a <see cref="float"/>, the parameters (margin of error
		/// 			and ULP tolerance) of the <see cref="FuzzySingle"/> are used for the comparison. </remarks>
		///
		/// <param name="other">	The <see cref="float"/> to compare against. </param>
		///
		/// <returns>	Negative if the value of this instance is less than <paramref name="other"/>, 0 if they are equal, or
		/// 			positive if this is greater. </returns>
		public int CompareTo(float other)
		{
			if (Equals(other)) { return 0; }
			return _value < other ? -1 : 1;
		}

		/// <summary>	Greater-than comparison operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is larger than <paramref name="second"/>; otherwise, false. </returns>
		public static bool operator >(FuzzySingle first, FuzzySingle second)
		{
			return first.CompareTo(second) > 0;
		}

		/// <summary>	Greater-than comparison operator for comparing a <see cref="FuzzySingle"/> to a <see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is larger than <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator >(FuzzySingle first, float second)
		{
			return first.CompareTo(second) > 0;
		}

		/// <summary>	Greater-than comparison operator for comparing a <see cref="float"/> to a <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is larger than the value of <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator >(float first, FuzzySingle second)
		{
			return second.CompareTo(first) < 0;
		}

		/// <summary>	Greater-than-or-equal comparison operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is larger than or equal to <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator >=(FuzzySingle first, FuzzySingle second)
		{
			return first.CompareTo(second) >= 0;
		}

		/// <summary>	Greater-than-or-equal comparison operator for comparing a <see cref="FuzzySingle"/> to a
		/// 			<see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is larger than or equal to <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator >=(FuzzySingle first, float second)
		{
			return first.CompareTo(second) >= 0;
		}

		/// <summary>	Greater-than-or-equal comparison operator for comparing a <see cref="float"/> to a
		/// 			<see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is larger than or equal to the value of <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator >=(float first, FuzzySingle second)
		{
			return second.CompareTo(first) <= 0;
		}

		/// <summary>	Less-than comparison operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is smaller than <paramref name="second"/>; otherwise, false. </returns>
		public static bool operator <(FuzzySingle first, FuzzySingle second)
		{
			return first.CompareTo(second) < 0;
		}

		/// <summary>	Less-than comparison operator for comparing a <see cref="FuzzySingle"/> to a <see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is smaller than <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator <(FuzzySingle first, float second)
		{
			return first.CompareTo(second) < 0;
		}

		/// <summary>	Less-than comparison operator for comparing a <see cref="float"/> to a <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is smaller than the value of <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator <(float first, FuzzySingle second)
		{
			return second.CompareTo(first) > 0;
		}

		/// <summary>	Less-than-or-equal comparison operator for comparing two <see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is smaller than or equal to <paramref name="second"/>; otherwise,
		/// 			false. </returns>
		public static bool operator <=(FuzzySingle first, FuzzySingle second)
		{
			return first.CompareTo(second) <= 0;
		}

		/// <summary>	Less-than-or-equal comparison operator for comparing a <see cref="FuzzySingle"/> to a
		/// 			<see cref="float"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if the value of <paramref name="first"/> is smaller than or equal to <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator <=(FuzzySingle first, float second)
		{
			return first.CompareTo(second) <= 0;
		}

		/// <summary>	Less-than-or-equal comparison operator for comparing a <see cref="float"/> to a
		/// 			<see cref="FuzzySingle"/>. </summary>
		///
		/// <param name="first"> 	The first operand. </param>
		/// <param name="second">	The second operand. </param>
		///
		/// <returns>	true if <paramref name="first"/> is smaller than or equal to the value of <paramref name="second"/>;
		/// 			otherwise, false. </returns>
		public static bool operator <=(float first, FuzzySingle second)
		{
			return second.CompareTo(first) >= 0;
		}

		#endregion

		#region Casts

		/// <summary>	Implicit cast that converts the given <see cref="FuzzySingle"/> to a <see cref="FuzzyDouble"/>. </summary>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	A <see cref="FuzzyDouble"/> matching <paramref name="fuzzySingle"/>. </returns>
		public static implicit operator FuzzyDouble(FuzzySingle fuzzySingle)
		{
			return new FuzzyDouble(fuzzySingle.Value, fuzzySingle.MarginOfError, fuzzySingle.ULPTolerance);
		}

		/// <summary>	Explicit cast that converts the given <see cref="FuzzySingle"/> to a <see cref="double"/>. </summary>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	The value of <paramref name="fuzzySingle"/> as a <see cref="double"/>. </returns>
		public static explicit operator double(FuzzySingle fuzzySingle)
		{
			return fuzzySingle._value;
		}

		/// <summary>	Explicit cast that converts the given <see cref="FuzzySingle"/> to a <see cref="float"/>. </summary>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	The value of <paramref name="fuzzySingle"/> as a <see cref="float"/>. </returns>
		public static explicit operator float(FuzzySingle fuzzySingle)
		{
			return fuzzySingle._value;
		}

		/// <summary>	Explicit cast that converts the given <see cref="FuzzySingle"/> to a <see cref="long"/>. </summary>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	The value of <paramref name="fuzzySingle"/> as a <see cref="long"/>. </returns>
		public static explicit operator long(FuzzySingle fuzzySingle)
		{
			return (long)fuzzySingle._value;
		}

		/// <summary>	Explicit cast that converts the given <see cref="FuzzySingle"/> to an <see cref="int"/>. </summary>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	The value of <paramref name="fuzzySingle"/> as an <see cref="int"/>. </returns>
		public static explicit operator int(FuzzySingle fuzzySingle)
		{
			return (int)fuzzySingle._value;
		}

		/// <summary>	Explicit cast that converts the given <see cref="FuzzySingle"/> to a <see cref="bool"/>. </summary>
		///
		/// <remarks>	Uses fuzzy comparison to determine the <see cref="bool"/> equivalent of the value. </remarks>
		///
		/// <param name="fuzzySingle">	The <see cref="FuzzySingle"/> to convert. </param>
		///
		/// <returns>	The value of <paramref name="fuzzySingle"/> as a <see cref="bool"/>. </returns>
		public static explicit operator bool(FuzzySingle fuzzySingle)
		{
			return !fuzzySingle.Equals(0.0f);
		}

		#endregion
	}
}
