﻿namespace FuzzyLogic
{
	/// <summary>	Detailed information about a <see cref="float"/>. </summary> 
	public struct SingleInfo
	{
		private readonly float _value;
		private readonly int _bits;

		/// <summary>	Constructs a new <see cref="SingleInfo"/> for <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value to obtain info about. </param>
		public SingleInfo(float value)
		{
			_value = value;
			_bits = SingleToInt32Bits(value);
		}

		/// <summary>	The stored value. </summary>
		///
		/// <value>	The stored value. </value>
		public float Value
		{
			get { return _value; }
		}

		/// <summary>	The underlying bit representation of the value, returned as an <see cref="int"/>. </summary>
		///
		/// <value>	The underlying bit representation of the value, returned as an <see cref="int"/>. </value>
		public int Bits
		{
			get { return _bits; }
		}

		/// <summary>	Gets a flag indicating whether the value is negative. </summary>
		///
		/// <value>	true if the value is negative, false if not. </value>
		public bool IsNegative
		{
			get { return (_bits < 0); }
		}

		/// <summary>	The exponent component of the value. </summary>
		///
		/// <value>	The exponent. </value>
		public int Exponent
		{
			get { return (_bits >> 23) & 0xff; }
		}

		/// <summary>	The mantissa component of the value. </summary>
		///
		/// <value>	The mantissa. </value>
		public int Mantissa
		{
			get { return _bits & 0x7fffff; }
		}

		/// <summary>	Converts a <see cref="float"/> into an <see cref="int"/>, preserving the exact binary representation of
		/// 			the <see cref="float"/>. </summary>
		///
		/// <param name="value">	The value to convert. </param>
		///
		/// <returns>	An <see cref="int"/> that stores the exact binary representation of the given value. </returns>
		public static int SingleToInt32Bits(float value)
		{
			// This allows us to perform the same operations as BitConverter, but without allocating a new array of bytes.
			return new SingleUnion(value).IntValue;
		}
	}
}
