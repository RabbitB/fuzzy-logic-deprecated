﻿using System;
using System.Runtime.InteropServices;
using FuzzyLogic.Properties;

namespace FuzzyLogic
{
	/// <summary>	Acts as a C union between a <see cref="float"/> and other 32-bit values. </summary>
	///
	/// <remarks>	Set one value, and you can retrieve the others as the raw binary representation of the set value. </remarks>
	[StructLayout(LayoutKind.Explicit)]
	public struct SingleUnion
	{
		/// <summary>	The stored value as a <see cref="float"/>. </summary>
		[FieldOffset(0)] public readonly float FloatValue;

		/// <summary>	The stored value as an <see cref="int"/>. </summary>
		[FieldOffset(0)] public readonly int IntValue;

		/// <summary>	The stored value as a <see cref="uint"/>. </summary> 
		[FieldOffset(0)] public readonly uint UIntValue;

		/// <summary>	Constructs a <see cref="SingleUnion"/> with a <see cref="float"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as a <see cref="float"/>. </param>
		public SingleUnion(float value) : this()
		{
			FloatValue = value;
		}

		/// <summary>	Constructs a <see cref="SingleUnion"/> with an <see cref="int"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as an <see cref="int"/>. </param>
		public SingleUnion(int value) : this()
		{
			IntValue = value;
		}

		/// <summary>	Constructs a <see cref="SingleUnion"/> with a <see cref="uint"/> <paramref name="value"/>. </summary>
		///
		/// <param name="value">	The value of this union as a <see cref="uint"/>. </param>
		public SingleUnion(uint value) : this()
		{
			UIntValue = value;
		}

		/// <summary>	Breaks the stored value down into its representative <see cref="byte"/> array. </summary>
		///
		/// <remarks>	The use of a pre-allocated buffer allows this method to function without allocating any data.
		/// 			<see cref="BitConverter.GetBytes(Single)"/> will always allocate a new <see cref="byte"/> array, which
		/// 			is why this method was written. </remarks>
		///
		/// <exception cref="ArgumentException">	Thrown when the size of the supplied <see cref="byte"/> buffer is smaller
		/// 										than the required size. </exception>
		///
		/// <param name="buffer">	The <see cref="byte"/> buffer to fill. </param>
		///
		/// <returns>	The <see cref="byte"/> buffer filled with the value broken into bytes. </returns>
		public byte[] GetBytes(byte[] buffer)
		{
			if (buffer.Length < sizeof(float))
			{
				throw new ArgumentException(
					String.Format(ExceptionStrings.SingleUnion_BufferNotLargeEnough, buffer.Length, sizeof(float)),
					"buffer");
			}

			buffer[0] = (byte)(IntValue & 0xff);
			buffer[1] = (byte)((IntValue >> 8) & 0xff);
			buffer[2] = (byte)((IntValue >> 16) & 0xff);
			buffer[3] = (byte)((IntValue >> 24) & 0xff);

			return buffer;
		}
	}
}
