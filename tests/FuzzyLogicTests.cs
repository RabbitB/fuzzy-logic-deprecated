﻿using System;
using NUnit.Framework;

namespace FuzzyLogic
{
	[TestFixture]
	[Category("FuzzyLogic Tests")]
	internal class FuzzyLogicTests
	{
		public Random NumberGen;

		[SetUp]
		public void Init()
		{
			System.Threading.Thread.Sleep(1);

			NumberGen = new Random();
			WarmupRandom(NumberGen);
		}

		// Determination tests never fail, as they test known (and expected) behavior, and thus exist to determine the 
		// characteristics of the behavior.
		#region Determination Tests

		[Test, Description("Calculates the error between two (initially equal) doubles, after arithmetic has been performed on one.")]
		[Repeat(5)]
		public void DetermineDoubleArithmeticError()
		{
			double originalValue = NumberGen.NextDouble() * NumberGen.Next();
			double calculatedValue = PerformEquivalentArithmetic(originalValue);

			UnityEngine.Debug.Log(String.Format(
				"Original: {0}, Calculated: {1} -- Error: {2}, % Error: {3}% -- ULP Difference: {4}",
				originalValue,
				calculatedValue,
				Math.Abs(originalValue - calculatedValue),
				(Math.Abs(originalValue - calculatedValue) / originalValue) * 100,
				FuzzyLogic.FuzzyCompare.ULPDistance(originalValue, calculatedValue)
			));
		}

		[Test, Description("Calculates the error between two (initially equal) floats, after arithmetic has been performed on one.")]
		[Repeat(5)]
		public void DetermineSingleArithmeticError()
		{
			float originalValue = (float)NumberGen.NextDouble() * NumberGen.Next();
			float calculatedValue = PerformEquivalentArithmetic(originalValue);

			UnityEngine.Debug.Log(String.Format(
				"Float -- Original: {0}, Calculated: {1} -- Error: {2}, % Error: {3}% -- ULP Difference: {4}",
				originalValue,
				calculatedValue,
				Math.Abs(originalValue - calculatedValue),
				(Math.Abs(originalValue - calculatedValue) / originalValue) * 100,
				FuzzyLogic.FuzzyCompare.ULPDistance(originalValue, calculatedValue)
			));
		}

		[Test, Description("If two fuzzy doubles are equal, this checks if they're also equal as hashed fuzzy doubles.")]
		[Repeat(50)]
		public void DetermineHashedDoubleEqualityVarianceAgainstNonHashed()
		{
			double originalValue = NumberGen.NextDouble() * NumberGen.Next();
			double calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzyHashedDouble hashedFuzzyOriginal = new FuzzyLogic.FuzzyHashedDouble(originalValue, 1E-6, 4);
			FuzzyLogic.FuzzyHashedDouble hashedFuzzyCalculated = new FuzzyLogic.FuzzyHashedDouble(calculatedValue, 1E-6, 4);

			FuzzyLogic.FuzzyDouble fuzzyOriginal = new FuzzyLogic.FuzzyDouble(hashedFuzzyOriginal);
			FuzzyLogic.FuzzyDouble fuzzyCalculated = new FuzzyLogic.FuzzyDouble(hashedFuzzyCalculated);

			if (fuzzyOriginal == fuzzyCalculated)
			{
				if (hashedFuzzyOriginal != hashedFuzzyCalculated)
				{
					UnityEngine.Debug.Log(String.Format(
						"Hashed fuzzy doubles failed to equate to equal, when non-hashed fuzzy doubles equated to equal."));
				}
			}
			else
			{
				UnityEngine.Debug.Log(String.Format("Fuzzy doubles not equal!"));
			}
		}

		[Test, Description("If two fuzzy floats are equal, this checks if they're also equal as hashed fuzzy floats.")]
		[Repeat(50)]
		public void DetermineHashedSingleEqualityVarianceAgainstNonHashed()
		{
			float originalValue = (float)NumberGen.NextDouble() * NumberGen.Next();
			float calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzyHashedSingle hashedFuzzyOriginal = new FuzzyLogic.FuzzyHashedSingle(originalValue, 1E-6f, 4);
			FuzzyLogic.FuzzyHashedSingle hashedFuzzyCalculated = new FuzzyLogic.FuzzyHashedSingle(calculatedValue, 1E-6f, 4);

			FuzzyLogic.FuzzySingle fuzzyOriginal = new FuzzyLogic.FuzzySingle(hashedFuzzyOriginal);
			FuzzyLogic.FuzzySingle fuzzyCalculated = new FuzzyLogic.FuzzySingle(hashedFuzzyCalculated);

			if (fuzzyOriginal == fuzzyCalculated)
			{
				if (hashedFuzzyOriginal != hashedFuzzyCalculated)
				{
					UnityEngine.Debug.Log(String.Format(
						"Hashed fuzzy floats failed to equate to equal, when non-hashed fuzzy floats equated to equal."));
				}
			}
			else
			{
				UnityEngine.Debug.Log(String.Format("Fuzzy floats not equal!"));
			}
		}

		#endregion

		#region Assertion Tests

		[Test, Description("Asserts that close to equal doubles, equate to equal when using fuzzy equality.")]
		[Repeat(50)]
		public void DoublesAreEqual()
		{
			double originalValue = NumberGen.NextDouble() * NumberGen.Next();
			double calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzyDouble fuzzyOriginal = new FuzzyLogic.FuzzyDouble(originalValue, 1E-6, 4);
			FuzzyLogic.FuzzyDouble fuzzyCalculated = new FuzzyLogic.FuzzyDouble(calculatedValue, 1E-6, 4);

			Assert.That(fuzzyOriginal == fuzzyCalculated);
		}

		[Test, Description("Asserts that close to equal floats, equate to equal when using fuzzy equality.")]
		[Repeat(50)]
		public void SinglesAreEqual()
		{
			float originalValue = (float)NumberGen.NextDouble() * NumberGen.Next();
			float calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzySingle fuzzyOriginal = new FuzzyLogic.FuzzySingle(originalValue, 1E-6f, 4);
			FuzzyLogic.FuzzySingle fuzzyCalculated = new FuzzyLogic.FuzzySingle(calculatedValue, 1E-6f, 4);

			Assert.That(fuzzyOriginal == fuzzyCalculated);
		}

		[Test, Description("Asserts that two hashed fuzzy doubles that equate to equal, also have equivalent hash codes.")]
		[Repeat(50)]
		public void EqualHashedDoublesHaveEqualHashCodes()
		{
			double originalValue = NumberGen.NextDouble() * NumberGen.Next();
			double calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzyHashedDouble hashedFuzzyOriginal = new FuzzyLogic.FuzzyHashedDouble(originalValue, 1E-6, 4);
			FuzzyLogic.FuzzyHashedDouble hashedFuzzyCalculated = new FuzzyLogic.FuzzyHashedDouble(calculatedValue, 1E-6, 4);

			if (hashedFuzzyOriginal == hashedFuzzyCalculated)
			{
				Assert.That(hashedFuzzyOriginal.GetHashCode() == hashedFuzzyCalculated.GetHashCode());
			}
		}

		[Test, Description("Asserts that two hashed fuzzy floats that equate to equal, also have equivalent hash codes.")]
		[Repeat(50)]
		public void EqualHashedSinglesHaveEqualHashCodes()
		{
			float originalValue = (float)NumberGen.NextDouble() * NumberGen.Next();
			float calculatedValue = PerformEquivalentArithmetic(originalValue);

			FuzzyLogic.FuzzyHashedSingle hashedFuzzyOriginal = new FuzzyLogic.FuzzyHashedSingle(originalValue, 1E-6f, 4);
			FuzzyLogic.FuzzyHashedSingle hashedFuzzyCalculated = new FuzzyLogic.FuzzyHashedSingle(calculatedValue, 1E-6f, 4);

			if (hashedFuzzyOriginal == hashedFuzzyCalculated)
			{
				Assert.That(hashedFuzzyOriginal.GetHashCode() == hashedFuzzyCalculated.GetHashCode());
			}
		}

		#endregion

		#region Helper Functions

		public void WarmupRandom(Random random)
		{
			for (int j = 0; j < 12; j++)
			{
				random.Next();
			}

			for (int j = 0; j < 12; j++)
			{
				random.NextDouble();
			}
		}

		public double PerformEquivalentArithmetic(double sourceValue)
		{
			double calculatedValue = sourceValue;

			double multiplyValue = NumberGen.NextDouble();
			double divisionValue = NumberGen.NextDouble();
			double additionValue = NumberGen.NextDouble();
			double subtractionValue = NumberGen.NextDouble();

			int steps = NumberGen.Next(1, 27);
			double steppedValue = 0;

			calculatedValue *= multiplyValue;
			calculatedValue += additionValue;
			calculatedValue /= divisionValue;
			calculatedValue *= multiplyValue;
			calculatedValue -= subtractionValue;

			for (int i = 0; i < steps; i++)
			{
				steppedValue += calculatedValue / steps;
			}

			steppedValue += subtractionValue;
			steppedValue /= multiplyValue;
			steppedValue *= divisionValue;
			steppedValue -= additionValue;
			steppedValue /= multiplyValue;

			return steppedValue;
		}

		public float PerformEquivalentArithmetic(float sourceValue)
		{
			float calculatedValue = sourceValue;

			float multiplyValue = (float)NumberGen.NextDouble();
			float divisionValue = (float)NumberGen.NextDouble();
			float additionValue = (float)NumberGen.NextDouble();
			float subtractionValue = (float)NumberGen.NextDouble();

			int steps = NumberGen.Next(1, 27);
			float steppedValue = 0;

			calculatedValue *= multiplyValue;
			calculatedValue += additionValue;
			calculatedValue /= divisionValue;
			calculatedValue *= multiplyValue;
			calculatedValue -= subtractionValue;

			for (int i = 0; i < steps; i++)
			{
				steppedValue += calculatedValue / steps;
			}

			steppedValue += subtractionValue;
			steppedValue /= multiplyValue;
			steppedValue *= divisionValue;
			steppedValue -= additionValue;
			steppedValue /= multiplyValue;

			return steppedValue;
		}

		#endregion
	}
}